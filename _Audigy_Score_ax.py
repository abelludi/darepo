"""
VERSION: 2.0
screens and score subset of Audigy locations with Acxiom data based of parameters listed below 
"""


############ PARAMETERS 

ROOT_FOLDER = '/media/analytics/Clients/DSR_Tasks/_Audigy'
FOLDER = '/4549_Audigy'
DSRID = 'DSR-4549'
MIN_AGE =65
MAX_AGE = 96
smart_radius = True
MAX_DIST = 50
ISSEGMENT = False # true if multiple locations
ISPATIENTS = True
patient_join_schema = "'{DSRID}'".format(DSRID=DSRID)
loc_join_schema = "'{DSRID}'".format(DSRID=DSRID)
loc_latlon_schema = ['loc_lat', 'loc_lon']
#######################
PROD_MODEL_PATH = '/media/analytics/Clients/__ProductionModels/acxiom'
CUSTOM_MODEL = False
CUSTOM_MODEL_PATH = "/media/analytics/Clients/DSR_Tasks/4120_Audigy_AVG/model_objects/Audigy_AVG_ax/Audigy_Good_Models_No_Data.pkl"
FALLBACK_MODEL = 'National'
FALLBACK_MODEL_PATH = PROD_MODEL_PATH + '/model_objects/Beltone_acxiom/BT_Ax_Good_Models_No_Data.pkl'
CLIENT = 'Audigy'
EXTRA_FEATURES = ['dist']
IS_ACXIOM = True
TABLE = 'dw_acxiom.fact_acxiom_last_month'
SEQ_ID = 'acxiom_sequence'
AGE='age_in_twotoyear_increments_input_individual'
loc_name_schema = 'client'
ENV = 'production'
DB_NAME = 'darwill1-olap.cm1xc01focet.us-east-1.redshift.amazonaws.com'

############ PARAMETERS 
if IS_ACXIOM:
  os.chdir(PROD_MODEL_PATH)
  from model_library import data as d
  from model_library import trainer as t
  from model_library import screener as s
  from model_library import scorer
  from model_library import smart_radius as sr
  from model_library import boto as bt

  os.chdir(ROOT_FOLDER)
  import pandas as pd, pickle, time
  from config import *
  cf = config()
  contact_fields = d.Data.CONTACT_FIELDS
  base_features = d.Data.BASE_FEATURES
  attributes = d.Data.list_to_str(list(set(contact_fields + base_features)))

SUPPRESSION_TABLE = '''(SELECT * FROM dw_audigy.fact_suppression)''' ## UPDATE 
if ISPATIENTS:
  PATIENTS = ''' (select * from dw_audigy.fact_contact where dsr in ('{DSRID}')) '''.format(DSRID=DSRID)
  PATIENTS
  print('Patients query built!\n Please review query below:\n' + PATIENTS)
  time.sleep(5)
else:
  print('No patients declared...\n Skipping Screening Phase')
    ### UPDATE TO YOUR LOCATIONS ###
if ISSEGMENT:
    LOCATIONS = ''' (select * from dw_sycle.fact_clinics where clinic_id in ('4-2589-8936', '4-2607-8977','4-535-11078','4-2716-9215',
                                                                             '4-2848-9662','4-274-8400','4-3103-10267','4-274-794',
                                                                             '4-535-1333','4-2692-9157','4-2721-9246','4-803-2139',
                                                                             '4-3103-10268','4-2606-8976','4-2721-9245','4-2716-9216',
                                                                             '4-2716-9217', '4-2179-8163')) '''
    print('Locations query built!\n\n')
    time.sleep(2)
    print('Please review query below:\n')
    time.sleep(2)
    print(LOCATIONS)
    time.sleep(5)
    print('Proceeding with locations query')

else:
    ### FOR SINGLE LOCATION ###
    locs = {1:{
      'ClientName': 'Audigy Szalc Dansville'
    , 'ClientAdd1':  '60 Red Jacket St'
    , 'ClientAdd2': 'Ste 16'
    , 'ClientCity': 'Dansville'
    , 'ClientState': 'NY' 
    , 'ClientZip': '14437' 
    , 'ClientLat': 42.552497
    , 'ClientLong': -77.695795
    },
    2:{
      'ClientName': 'Audigy Szalc Hornell'
    , 'ClientAdd1':  '138 Main St'
    , 'ClientAdd2': ''
    , 'ClientCity': 'Hornell'
    , 'ClientState': 'NY' 
    , 'ClientZip': '14843' 
    , 'ClientLat': 42.327158
    , 'ClientLong': -77.65979
    },
    }
    #clean up temp_phonak_stores for this ticket
    res = pd.read_sql_query("delete from datascience.temp_phonak_stores where dsr_id = '{TICKET_ID}';select * from datascience.temp_phonak_stores where dsr_id = '{TICKET_ID}'".format(TICKET_ID=DSRID),cf.RD_CON)
    for thisloc in locs:
        loc = locs[thisloc]
        rungry = "insert into datascience.temp_phonak_stores (select '{TICKET_ID}','{CLIENT_NAME}','{CLIENT_ADDRESS1}','{CLIENT_ADDRESS2}','{CLIENT_CITY}','{CLIENT_STATE}','{CLIENT_ZIP}',{CLIENT_LAT},{CLIENT_LONG});Select * from datascience.temp_phonak_stores where dsr_id = '{TICKET_ID}'".format(TICKET_ID=DSRID,CLIENT_NAME=loc['ClientName'],CLIENT_ADDRESS1=loc['ClientAdd1'],CLIENT_ADDRESS2=loc['ClientAdd2'],CLIENT_CITY=loc['ClientCity'],CLIENT_STATE=loc['ClientState'],CLIENT_ZIP=loc['ClientZip'],CLIENT_LAT=loc['ClientLat'],CLIENT_LONG=loc['ClientLong'])
        res = pd.read_sql_query(rungry, cf.RD_CON)
    assert res.shape[0] == len(locs),"Problems with the datascience.temp_phonak_stores"
    print('Locations dictionary built!\n\n')
    time.sleep(2)
    print('Please review dictionary below:\n\n' + rungry)
    time.sleep(5)
    print('Proceeding with phonak temp table creation using locations dictionary...\n\n')
    time.sleep(2)
    print('locations table built!\n\n Please review location table below:\n')
    print(res)
    time.sleep(5)
    LOCATIONS = "(select * from datascience.temp_phonak_stores where dsr_id = '{DSRID}')".format(DSRID=DSRID)
  
LOCATIONS
res

#################### QUERIES (Just Review - Modify only if necessary)
if ISPATIENTS:
  screen_query = """
  drop table if exists #patients;
  create temp table #patients as
  select 'DSR-4549' as join_id, * from {PATIENTS};

  drop table if exists #locs;
  create temp table #locs as 
  select 'DSR-4549' as join_id, * from {LOCATIONS};

  drop table if exists #cust;
  create temp table #cust as
  (select distinct a.{SEQ_ID} as seq_id, p_geo_zip_code as zip, {AGE} as age
          , b.join_id, b.city || '_' || b.address as loc
          , dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}::float, b.{lon}::float) as dist
  from  {TABLE} a
  join #patients z
          on lower(trim(z.first_name)) = lower(trim(a.p_dem_first_name)) and 
          lower(trim(z.last_name)) = lower(trim(a.p_dem_last_name)) and 
          lower(trim(z.out_addressline1)) = lower(trim(a.p_geo_address_1)) and
          substring(lower(trim(z.out_zip5)), 1, 5) = lower(trim(a.p_geo_zip_code))
          join #locs b
          on z.join_id = b.join_id
  where dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}::float, b.{lon}::float) <= 50
  and ({AGE} >={MIN_AGE} and {AGE} <= {MAX_AGE}));

  drop table if exists #cust;
  create temp table #cust as
  (select distinct a.{SEQ_ID} as seq_id, p_geo_zip_code as zip, {AGE} as age
          , b.join_id, b.city || '_' || b.address as loc
          , dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}::float, b.{lon}::float) as dist
  from  {TABLE} a
  join #patients z
          on lower(trim(z.first_name)) = lower(trim(a.p_dem_first_name)) and 
          lower(trim(z.last_name)) = lower(trim(a.p_dem_last_name)) and 
          substring(lower(trim(z.out_addressline1)),1,10) = substring(lower(trim(a.p_geo_address_1)),1,10) and
          substring(lower(trim(z.out_zip5)), 1, 5) = lower(trim(a.p_geo_zip_code))
          join #locs b
          on z.join_id = b.join_id
  where dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}::float, b.{lon}::float) <= 50
  and ({AGE} >={MIN_AGE} and {AGE} <= {MAX_AGE}));

  drop table if exists #territory;
  create temp table #territory as
  (select a.join_id, city || '_' || address as loc, a.state, 
                             {lat},
                             {lon},
                            latlon_limits({lat}, {lon}, 50, 1) as minlat,
                            latlon_limits({lat}, {lon}, 50, 2) as maxlat,
                            latlon_limits({lat}, {lon}, 50, 3) as minlon,
                            latlon_limits({lat}, {lon}, 50, 4) as maxlon,
                            b.adjacentstate  
                    from #locs a
                    inner join datascience.temp_#adjacent_states b
                    on a.state = b.state
  );

  drop table if exists #mailable_pop;
  create temp table #mailable_pop as
  (select distinct a.{SEQ_ID} as seq_id, 
                          dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}, b.{lon}) as dist, b.join_id, b.loc, a.{AGE},a.p_geo_state,
                          row_number() over(partition by a.{SEQ_ID} order by dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}, b.{lon}) asc) as rk
                  from {TABLE} a
                  inner join #territory b
                  on a.p_geo_state = b.adjacentstate
                  where ({AGE} >={MIN_AGE} and {AGE} <= {MAX_AGE})
                  and (a.p_geo_latitude_ic between b.minlat and b.maxlat)
                  and (a.p_geo_longitude_ic between b.minlon and b.maxlon)
                  and p_geo_zip_code in (select distinct zip from #cust));

  drop table if exists #unique_pop;
  create temp table #unique_pop as
  (select distinct seq_id, dist, {AGE} as age, join_id, loc, p_geo_state as p_state from #mailable_pop where rk=1 and dist <= {MAX_DIST});

  drop table if exists #final_pop;
  create temp table #final_pop as
  select distinct {LABEL} as target, 
                  {ATTRIBUTES}, coalesce(b.dist, z.dist) as dist,  coalesce(b.join_id, z.join_id) as loc_id, coalesce(b.loc, z.loc) as loc,  z.p_state 
  from {TABLE} a
  {HOW} join #cust b
      on a.{SEQ_ID} = b.seq_id
  join #unique_pop z
      on a.{SEQ_ID} = z.seq_id
  {EXTRA}
  {DECEASED};

  select * from #final_pop
  {EXTRA2}
      """



  extra = """
  where b.seq_id is null 
  and (age_in_twotoyear_increments_input_individual >={MIN_AGE} and age_in_twotoyear_increments_input_individual <= {MAX_AGE})
  and p_geo_zip_code in ({{zipcodes}})
  """.format(MIN_AGE=MIN_AGE, MAX_AGE=MAX_AGE)

  extra2 = '''
  where (age_in_twotoyear_increments_input_individual >= {MIN_AGE} and age_in_twotoyear_increments_input_individual <= {MAX_AGE})  {{balance_by}}  
  order by random()
  limit {{size}}
  '''.format(MIN_AGE=MIN_AGE, MAX_AGE=MAX_AGE)

  class_1 = screen_query.format(TABLE = TABLE,LABEL=1, SEQ_ID=SEQ_ID,ATTRIBUTES=attributes, HOW='inner', EXTRA='', EXTRA2='', LOCATIONS = LOCATIONS, AGE=AGE, MIN_AGE=MIN_AGE, MAX_AGE=MAX_AGE, PATIENTS=PATIENTS, DECEASED = '', patient_join_schema=patient_join_schema, loc_join_schema=loc_join_schema,lat=loc_latlon_schema[0], lon=loc_latlon_schema[1], MAX_DIST=MAX_DIST)
  class_0 = screen_query.format(TABLE = TABLE,LABEL=0, SEQ_ID=SEQ_ID,ATTRIBUTES=attributes, HOW='left', EXTRA=extra, EXTRA2=extra2, LOCATIONS = LOCATIONS, AGE=AGE, MIN_AGE=MIN_AGE, MAX_AGE=MAX_AGE, PATIENTS=PATIENTS, DECEASED = '', patient_join_schema=patient_join_schema, loc_join_schema=loc_join_schema, lat=loc_latlon_schema[0], lon=loc_latlon_schema[1], MAX_DIST=MAX_DIST)


  score_query = """
  drop table if exists #patients;
  create temp table #patients as
  select 'DSR-4549' as join_id, * from {PATIENTS};

  drop table if exists #locs;
  create temp table #locs as 
  select 'DSR-4549' as join_id, * from {LOCATIONS};

  drop table if exists #cust;
  create temp table #cust as
  (select distinct a.{SEQ_ID} as seq_id, p_geo_zip_code as zip, {AGE} as age
          , b.join_id, b.city || '_' || b.address as loc
          , dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}::float, b.{lon}::float) as dist
  from  {TABLE} a
  join #patients z
          on lower(trim(z.first_name)) = lower(trim(a.p_dem_first_name)) and 
          lower(trim(z.last_name)) = lower(trim(a.p_dem_last_name)) and 
          lower(trim(z.out_addressline1)) = lower(trim(a.p_geo_address_1)) and
          substring(lower(trim(z.out_zip5)), 1, 5) = lower(trim(a.p_geo_zip_code))
          join #locs b
          on z.join_id = b.join_id
  where dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}::float, b.{lon}::float) <= 50
  and ({AGE} >={MIN_AGE} and {AGE} <= {MAX_AGE}));

  drop table if exists #territory;
  create temp table #territory as
  (select a.join_id, city || '_' || address as loc, a.state, 
                             {lat},
                             {lon},
                            latlon_limits({lat}, {lon}, 50, 1) as minlat,
                            latlon_limits({lat}, {lon}, 50, 2) as maxlat,
                            latlon_limits({lat}, {lon}, 50, 3) as minlon,
                            latlon_limits({lat}, {lon}, 50, 4) as maxlon,
                            b.adjacentstate  
                    from #locs a
                    inner join datascience.temp_#adjacent_states b
                    on a.state = b.state
  );

  drop table if exists #mailable_pop;
  create temp table #mailable_pop as
  (select distinct a.{SEQ_ID} as seq_id, 
                          dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}, b.{lon}) as dist, b.join_id, b.loc, a.{AGE},a.p_geo_state,
                          row_number() over(partition by a.{SEQ_ID} order by dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}, b.{lon}) asc) as rk
                  from {TABLE} a
                  inner join #territory b
                  on a.p_geo_state = b.adjacentstate
                  where ({AGE} >={MIN_AGE} and {AGE} <= {MAX_AGE})
                  and (a.p_geo_latitude_ic between b.minlat and b.maxlat)
                  and (a.p_geo_longitude_ic between b.minlon and b.maxlon)
                  and p_geo_zip_code in (select distinct zip from #cust));

  drop table if exists #unique_pop;
  create temp table #unique_pop as
  (select distinct seq_id, dist, {AGE} as age, join_id, loc, p_geo_state as p_state from #mailable_pop where rk=1 and dist <= {MAX_DIST});

  drop table if exists #final_pop;
  create temp table #final_pop as
  select distinct {ATTRIBUTES}, coalesce(b.dist, z.dist) as dist,  coalesce(b.join_id, z.join_id) as loc_id, coalesce(b.loc, z.loc) as loc,  z.p_state, z.age, a.p_geo_latitude_ic, a.p_geo_longitude_ic 
  from {TABLE} a
  join #unique_pop z
      on a.{SEQ_ID} = z.seq_id
  left join #cust b
      on a.{SEQ_ID} = b.seq_id
  where z.seq_id is not null
  and b.seq_id is null
  and ({AGE} >={MIN_AGE} and {AGE} <= {MAX_AGE})
  {DECEASED};

  select * from #final_pop
      """



  score_query = score_query.format(TABLE=TABLE, SEQ_ID=SEQ_ID, ATTRIBUTES=attributes, LOCATIONS=LOCATIONS, AGE=AGE, DECEASED='', MIN_AGE=MIN_AGE, MAX_AGE=MAX_AGE, PATIENTS=PATIENTS, patient_join_schema=patient_join_schema, loc_join_schema=loc_join_schema, lat=loc_latlon_schema[0], lon=loc_latlon_schema[1], MAX_DIST=MAX_DIST)

########################### acxiom

  if IS_ACXIOM:    
      # Good models, no data for easy scoring
      class model_container:
          def __init__(self, name: str, models_good: dict):
              self.name = name
              self.models_good = models_good
      
      if CUSTOM_MODEL:
        print("Reading...")
        with open(CUSTOM_MODEL_PATH, "rb") as f:
            sessions = pickle.load(f)
      else:
        print("Reading...")
        with open(FALLBACK_MODEL_PATH, "rb") as f:
            sessions = pickle.load(f)


      # class_1 = screen_query.format(TABLE = 'dw_v12.v12',LABEL=1, ATTRIBUTES=attributes, HOW='inner', EXTRA='', LOCATIONS_TABLE = LOCATIONS, AGE='p_dem_age', MIN_AGE=MIN_AGE, MAX_AGE=MAX_AGE, PATIENTS=PATIENTS, DSR_ID=DSRID)
      # class_0 = screen_query.format(TABLE = 'dw_v12.v12',LABEL=0, ATTRIBUTES=attributes, HOW='left', EXTRA=extra, LOCATIONS_TABLE = LOCATIONS, AGE='p_dem_age', MIN_AGE=MIN_AGE, MAX_AGE=MAX_AGE, PATIENTS=PATIENTS, DSR_ID=DSRID)

      fallback_model = [k for (k,v) in sessions.models_good.items() if FALLBACK_MODEL in k][0]
      print('fallback_model: '+ fallback_model)
           
      print('Screening...')
      cf = config(db_name=DB_NAME)
      data = d.Data(name=CLIENT + '_Screen_Data', balance_by='loc', pos=class_1, neg=class_0, db_con=cf.RD_CON,
                        base_features=base_features, extra_features=EXTRA_FEATURES)
      screen = s.Screener(data, sessions, screen_by='loc', max_overfit=100, fallback_model=fallback_model)

      rad, rads = sr.smart_radius(data.df[data.df['target'] == 1], loc_field='loc')

      print('Missing locations - adding fall back model')
      query_m= '''select distinct a.city||'_'||a.address as loc from {LOCATIONS} a '''.format(LOCATIONS = LOCATIONS)
      locations = pd.read_sql_query(query_m, cf.RD_CON)
      for id in (set(locations['loc']) - set(screen.selected['__loc_key__'])):
              print(id)
              screen.selected = screen.selected.append({'__loc_key__' : id , '__model_key__' : fallback_model} , ignore_index=True)
              rad = rad.append({'loc':id, 'sr_80': rad['sr_80'].mean()}, ignore_index = True)
      rad
              
      print('Scoring...')
      # cf = config(db_name=DB_NAME)
      final_fields = contact_fields + ['p_geo_latitude_ic', 'p_geo_longitude_ic',
                                       'age', 'dist', 'loc', 'loc_id']
      os.getcwd()
      os.chdir(ROOT_FOLDER + FOLDER)
      if not os.path.exists(DSRID): 
          os.makedirs(DSRID)

      PATH = DSRID + '/scores_acxiom_' + str(DSRID)
      os.chdir(ROOT_FOLDER + FOLDER)
      if not os.path.exists(PATH): 
          os.makedirs(PATH)
          
      SAVE_PATH = ROOT_FOLDER +FOLDER+ '/' + PATH + '/'
      SAVE_PATH
      # import importlib
      # importlib.reload(scorer)

      fin_ax = scorer.Scorer().score_large_by_model(score_query,cf.RD_CON, screen.selected, sessions.models_good,final_fields=final_fields, client=CLIENT, save_path=SAVE_PATH,chunk_size=1000000)

      os.getcwd()
      os.chdir(ROOT_FOLDER)
      import importlib
      importlib.reload(bt)
      DSRID = DSRID.replace("-", "")
      bt.df_to_redshift(client="model_audigy_acxiom_lm_" + str(DSRID),path=SAVE_PATH,schema='datascience', is_timestamp=False)
      bt.df_to_redshift(client="model_SR_acxiom_lm_" + str(DSRID),df = rad,schema='datascience', is_timestamp=False)
      rad.to_csv(ROOT_FOLDER + FOLDER + '/model_SR_acxiom_lm_' + str(DSRID) + '.csv')



else:
  print('\n\nNo patients declared.. Proceeding with scoring\n\n')
  query = """
  drop table if exists #locs;
  create temp table #locs as 
  select * from {LOCATIONS};

  drop table if exists #territory;
  create temp table #territory as
  (select {loc_name_schema}, city || '_' || address as loc, a.state, 
                             {lat},
                             {lon},
                            latlon_limits({lat}, {lon}, 50, 1) as minlat,
                            latlon_limits({lat}, {lon}, 50, 2) as maxlat,
                            latlon_limits({lat}, {lon}, 50, 3) as minlon,
                            latlon_limits({lat}, {lon}, 50, 4) as maxlon,
                            b.adjacentstate  
                    from #locs a
                    inner join datascience.temp_#adjacent_states b
                    on a.state = b.state
  );

  drop table if exists #mailable_pop;
  create temp table #mailable_pop as
  (select distinct a.{SEQ_ID} as seq_id, 
                          dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}, b.{lon}) as dist, b.{loc_name_schema}, b.loc, a.{AGE},a.p_geo_state,
                          row_number() over(partition by a.{SEQ_ID} order by dist(a.p_geo_latitude_ic, a.p_geo_longitude_ic, b.{lat}, b.{lon}) asc) as rk
                  from {TABLE} a
                  inner join #territory b
                  on a.p_geo_state = b.adjacentstate
                  where ({AGE} >={MIN_AGE} and {AGE} <= {MAX_AGE})
                  and (a.p_geo_latitude_ic between b.minlat and b.maxlat)
                  and (a.p_geo_longitude_ic between b.minlon and b.maxlon));

  drop table if exists #unique_pop;
  create temp table #unique_pop as
  (select distinct seq_id, dist, {AGE} as age, {loc_name_schema}, loc, p_geo_state as p_state from #mailable_pop where rk=1 and dist <= {MAX_DIST});


  drop table if exists #final_pop;
  create temp table #final_pop as
  select distinct {ATTRIBUTES}, z.dist,  z.{loc_name_schema} as loc_id, z.loc as loc,  z.p_state, z.age, a.p_geo_latitude_ic, a.p_geo_longitude_ic 
  from {TABLE} a
  join #unique_pop z
      on a.{SEQ_ID} = z.seq_id
  where z.seq_id is not null
  and ({AGE} >={MIN_AGE} and {AGE} <= {MAX_AGE})
  {DECEASED};

  select * from #final_pop
      """



  query = query.format(TABLE=TABLE, SEQ_ID=SEQ_ID, ATTRIBUTES=attributes, LOCATIONS=LOCATIONS, AGE=AGE, DECEASED='', MIN_AGE=MIN_AGE, MAX_AGE=MAX_AGE, loc_name_schema=loc_name_schema, lat=loc_latlon_schema[0], lon=loc_latlon_schema[1], MAX_DIST=MAX_DIST)

########################### V12

  if IS_ACXIOM:    
      # Good models, no data for easy scoring
      class model_container:
          def __init__(self, name: str, models_good: dict):
              self.name = name
              self.models_good = models_good

      if CUSTOM_MODEL:
        print("Reading custom models...")
        with open(CUSTOM_MODEL_PATH, "rb") as f:
            sessions = pickle.load(f)
      else:
        print("Reading default production models...")
        with open(FALLBACK_MODEL_PATH, "rb") as f:
            sessions = pickle.load(f)


      fallback_model = [k for (k,v) in sessions.models_good.items() if FALLBACK_MODEL in k][0]
      print('fallback_model: '+ fallback_model)
           
      if ISSEGMENT:
        if ISPATIENTS:
          if smart_radius:
            print('Missing locations - adding fall back model')
            query_m= '''select distinct a.city||'_'||a.address as loc from {LOCATIONS} a '''.format(LOCATIONS = LOCATIONS)
            locations = pd.read_sql_query(query_m, cf.RD_CON)
            for id in (set(locations['loc']) - set(screen.selected['__loc_key__'])):
                      print(id)
                      screen.selected = screen.selected.append({'__loc_key__' : id , '__model_key__' : fallback_model} , ignore_index=True)
                      rad = rad.append({'loc':id, 'sr_80': rad['sr_80'].mean()}, ignore_index = True)
          else:
            print('Missing locations - adding fall back model')
            query_m= '''select distinct a.city||'_'||a.address as loc from {LOCATIONS} a '''.format(LOCATIONS = LOCATIONS)
            locations = pd.read_sql_query(query_m, cf.RD_CON)
            for id in (set(locations['loc']) - set(screen.selected['__loc_key__'])):
                      print(id)
                      screen.selected = screen.selected.append({'__loc_key__' : id , '__model_key__' : fallback_model} , ignore_index=True)
                      rad = rad.append({'loc':id, 'sr_80': rad['sr_80'].max()}, ignore_index = True)

      else:
        g_query = '''{LOCATIONS}'''.format(LOCATIONS=LOCATIONS)
        get_geocodes = pd.read_sql_query(g_query, cf.RD_CON)
        if smart_radius:
          rad = sr.smart_radius(get_geocodes)
          MAX_DIST = rad['sr'].max()
        else:
          # rad = sr.smart_radius(get_geocodes)
          rad = get_geocodes
          rad['sr']=MAX_DIST
              
      print('Scoring...')
      # cf = config(db_name=DB_NAME)
      final_fields = contact_fields + ['p_geo_latitude_ic', 'p_geo_longitude_ic',
                                       'age', 'dist', 'loc', 'loc_id']
      os.getcwd()
      os.chdir(ROOT_FOLDER + FOLDER)
      if not os.path.exists(DSRID): 
          os.makedirs(DSRID)

      PATH = DSRID + '/scores_acxiom_' + str(DSRID)
      os.chdir(ROOT_FOLDER + FOLDER)
      if not os.path.exists(PATH): 
          os.makedirs(PATH)
          
      SAVE_PATH = ROOT_FOLDER +FOLDER+ '/' + PATH + '/'
      SAVE_PATH
      # import importlib
      # importlib.reload(scorer)

      df = pd.read_sql_query(query, cf.RD_CON)
      df.shape
      df['dist'].max()

      model = sessions.models_good[fallback_model]
      q = scorer.Scorer().score_single(df, model=model, clf_name = fallback_model, final_fields=final_fields)
      fin = q[0]
      fin.shape
      fin.columns
      rad
      os.getcwd()
      import importlib
      importlib.reload(bt)
      bt.df_to_redshift(client="model_audigy_acxiom_" + str(DSRID),df = fin,schema='datascience', is_timestamp=False)
      bt.df_to_redshift(client="model_SR_acxiom_" + str(DSRID),df = rad,schema='datascience', is_timestamp=False)
      rad.to_csv(ROOT_FOLDER + FOLDER + '/model_SR_acxiom_' + str(DSRID) + '.csv')

 
