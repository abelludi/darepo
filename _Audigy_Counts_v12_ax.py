"""
VERSION: 2.0
Pulls counts and mailfiles for  Audigy locations with V12 and Acxiom data based of parameters listed below 
"""
 
ROOT_FOLDER = '/media/analytics/Clients/DSR_Tasks'
FOLDER = '/_Audigy/4549_Audigy'
import pandas as pd, pickle, os, sys, csv
os.chdir(ROOT_FOLDER)
from config import *
cf = config()
from model_library import wm_format as wmm
os.chdir(ROOT_FOLDER + FOLDER)

############ PARAMETERS 

DSRID = 'dsr4549'
CLIENT = 'Audigy'
MIN_AGE =65
MAX_AGE = 96
EXTRA_FEATURES = ['dist']
ENV = 'production'
DB_NAME = 'darwill1-olap.cm1xc01focet.us-east-1.redshift.amazonaws.com'
SUPPRESSION_TABLE = '''(SELECT * FROM dw_audigy.fact_suppression)''' ## UPDATE EVERY MONDAY
ISPATIENTS = True
PATIENTS = '''(select * from dw_audigy.fact_contact where dsr in ('{DSRID}'))'''.format(DSRID=DSRID) ## UPDATE IF DIFFERENT
COUNTS = True
ISMAILFILE = True
SMART_RADIUS = True
MAX_DIST = 50

if ISPATIENTS:
    PATIENTS1 = '''
        drop table if exists patients;
        create temp table patients as 
        (select distinct  MD5(
                              upper(trim(coalesce(first_name, '')))
                             +upper(trim(coalesce(last_name, '')))
                             +substring(upper(trim(coalesce(out_addressline1, ''))),1,10)
                             +substring(upper(trim(coalesce(out_zip5, ''))),1,5)) as hh_hash_supp from {PATIENTS});
    '''.format(PATIENTS=PATIENTS)
    EXTRA = 'left join patients z on trim(z.hh_hash_supp) = trim(a.patient_hash)'
    EXTRA2 = 'and z.hh_hash_supp is null'
else:
    PATIENTS1 = ''
    EXTRA = ''
    EXTRA2 = ''
    print('\n No Patients declared.. will not suppress in counts query')


############ PARAMETERS

if COUNTS: 
    
    counts_query ='''
        drop table if exists #scores; 
        create temp table #scores as
        select distinct '1.v12_and_acxiom' as type, a.acxiom_sequence as unique_id, a.ic_ind_group_id, a.ic_fam_group_id, a.score, a.score_range, a.loc, a.dist, a.age, a.p_dem_first_name,a.p_dem_last_name, a.p_geo_address_1, a.p_geo_address_2, a.p_geo_zip_code, a.p_geo_zip4, a.p_geo_city, a.p_geo_state
            from datascience.model_audigy_acxiom_lm_{DSRID} a join datascience.model_audigy_v12_{DSRID} b on a.ic_ind_group_id =b.ic_ind_group_id
        UNION
        select distinct '2.acxiom_only' as type, a.acxiom_sequence as unique_id, a.ic_ind_group_id, a.ic_fam_group_id, a.score, a.score_range,  a.loc, a.dist , a.age,a.p_dem_first_name, a.p_dem_last_name, a.p_geo_address_1, a.p_geo_address_2, a.p_geo_zip_code, a.p_geo_zip4 , a.p_geo_city, a.p_geo_state  
            from datascience.model_audigy_acxiom_lm_{DSRID} a left join datascience.model_audigy_v12_{DSRID} b on a.ic_ind_group_id =b.ic_ind_group_id where b.ic_ind_group_id is null
        UNION 
        select distinct '3.v12_only' as type, a.p_sys_sequence as unique_id, a.ic_ind_group_id, a.ic_fam_group_id, a.score, a.score_range, a.loc, a.dist, a.age, a.p_dem_first_name, a.p_dem_last_name, a.p_geo_address_1, a.p_geo_address_2, a.p_geo_zip_code  , a.p_geo_zip4, a.p_geo_city, a.p_geo_state  
             from datascience.model_audigy_v12_{DSRID} a left join datascience.model_audigy_acxiom_lm_{DSRID} b on a.ic_ind_group_id =b.ic_ind_group_id where b.ic_ind_group_id is null;
        
        drop table if exists #scores_hash;
        create temp table #scores_hash as
        (select distinct *, MD5((upper(trim(coalesce(p_dem_last_name, '')))+ substring(upper(trim(coalesce(p_geo_address_1, ''))),1,10) +upper(trim(coalesce(p_geo_zip_code, ''))))) as supp_hash,
        MD5(substring(upper(trim(coalesce(p_geo_address_1, ''))),1,10) +upper(trim(coalesce(p_geo_zip_code, '')))) as patient_hash 
        from #scores);

        {PATIENTS1}

        drop table if exists suppressions;
        create temp table suppressions as 
        (select distinct  MD5((upper(trim(coalesce(last_name, ''))))
                                +substring(upper(trim(coalesce(out_addressline1, ''))),1,10)
                                +substring(upper(trim(coalesce(out_zip5, ''))),1,5)) as hh_hash_supp,
                           out_addressline1, last_name, out_zip5 from {SUPPRESSION_TABLE});



        select a.* from #scores_hash a
        {EXTRA}
        left join suppressions z2
        on upper(trim(coalesce(a.p_geo_address_1, ''))) = upper(trim(coalesce(z2.out_addressline1, ''))) and 
        upper(trim(coalesce(a.p_dem_last_name, ''))) = upper(trim(coalesce(z2.last_name, ''))) and 
        upper(trim(coalesce(a.p_geo_zip_code, ''))) = upper(trim(coalesce(z2.out_zip5, '')))
        where z2.out_addressline1 is null and z2.last_name is null and z2.out_zip5 is null and z2.hh_hash_supp is null
        and unique_id is not null
        and a.age between {MIN_AGE} and {MAX_AGE}
        {EXTRA2} 
        

         
    '''.format(DSRID=DSRID, MIN_AGE=MIN_AGE, MAX_AGE=MAX_AGE, SUPPRESSION_TABLE=SUPPRESSION_TABLE, PATIENTS1=PATIENTS1, EXTRA=EXTRA, EXTRA2=EXTRA2)
    cf = config()
    df =  pd.read_sql_query(counts_query, cf.RD_CON)
    print(df.shape) # 85,850
    df.columns
    df['dist'].max()
    df['loc'].nunique()

    print('Housholding...')
    df = df.sort_values(by =['ic_fam_group_id', 'score'], ascending=[True, False])
    df_hh = df.drop_duplicates(subset =['ic_fam_group_id'], keep ='first')
    print(df_hh.shape) # 34,323

    
    print('Geography...')
    rad_v12 = pd.read_sql_query('select * from datascience.model_SR_v12_{DSRID}'.format(DSRID=DSRID), cf.RD_CON)
    rad_ax = pd.read_sql_query('select * from datascience.model_SR_acxiom_lm_{DSRID}'.format(DSRID=DSRID), cf.RD_CON)

    if SMART_RADIUS:
        df_geo = pd.DataFrame()
        for i in list(df_hh['loc'].unique()):
            print(i)
            subset = df_hh[df_hh['loc']==i]
            subset_both = subset[subset['type']=='1.v12_and_acxiom']
            subset_ax  = subset[subset['type']=='2.acxiom_only']
            subset_v12 = subset[subset['type']=='3.v12_only']
        
            subset_ax = subset_ax.merge(rad_ax, on='loc', how='left')
            subset_v12 = subset_v12.merge(rad_v12, on='loc', how='left')

            sample_size_ax = rad_ax[rad_ax['loc']==i]['sample_size']
            sample_size_v12 = rad_v12[rad_v12['loc']==i]['sample_size']
        
            if int(sample_size_ax) > int(sample_size_v12):
                subset_both = subset_both.merge(rad_ax, on='loc', how='left')
            else:
                subset_both = subset_both.merge(rad_v12, on='loc', how='left')

                df_geo = df_geo.append(subset_both)
                df_geo = df_geo.append(subset_ax)
                df_geo = df_geo.append(subset_v12)
                print(df_geo.shape)
        fin = df_geo[df_geo['dist']<=df_geo['sr_80']]
    else:
        fin = df_hh[df_hh['dist']<=MAX_DIST]
            

    
    fin.shape # 24,460
    fin['loc'].unique()
    fin.groupby('loc').size()
    fin['dist_range'] = fin['dist'].astype(int)


    print('Uploading scores to redshift...')   
    # sys.path.append('./model_library/')
    from model_library import boto as bt
    import importlib
    importlib.reload(bt)
    if 'index_ic' in fin.columns:
        fin.pop('index_ic')
    fin['dist'] = fin['dist'].fillna(0)
    fin['dist'] = fin['dist'].astype(float)
    fin['unique_id'] = fin['unique_id'].fillna(0)
    fin['unique_id'] = fin['unique_id'].astype(int)
    bt.df_to_redshift(client=DSRID + "_model_scores_v12_Acxiom", schema='datascience', df=fin, is_timestamp = True)



if ISMAILFILE:
    path4 = ROOT_FOLDER + FOLDER + '/mailfiles/'
    if not os.path.exists(path4):
        os.mkdir(path4)

    path5 = path4 + 'wm_format/'
    if not os.path.exists(path5):
        os.mkdir(path5)

    path6 = path4 + 'mailfiles/'
    if not os.path.exists(path6):
        os.mkdir(path6)

    final = pd.DataFrame()
    for loc in list(fin['loc'].unique()):
        subset = fin[fin['loc']==loc]
        wm, mapping = wmm.format_mailfile(subset)
        wm.to_csv(path5 + '_wm_format_' + str(loc) + '_mailfile.csv', index=False)
        subset.to_csv(path6 + str(loc) + '_mailfile.csv')
        final = final.append(subset)
        print('\n mailfile for' + str(loc) + 'saved to path:\n\n' + path4)




print('QA......')
os.chdir(ROOT_FOLDER + FOLDER)
from QA_SB import qa as qa
qa(final)

pd.crosstab(final['loc'], final['score_range']).to_csv(ROOT_FOLDER + FOLDER + 'counts_by_score_range.csv')


